# Moulimie

Moulimie est un caractère typographique dessiné par [Ricardo aka Johan](http://ricardoakajohan.be/)

Like a bread

## Specimen

![specimen1](Specimen1.jpg)
![specimen2](Specimen2.jpg)

## License

Moulimie is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at
http://scripts.sil.org/OFL